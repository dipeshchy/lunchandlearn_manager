# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users

  root 'talks#index'

  resources :talks do
    resources :references
  end

  get 'upcoming_talks' => 'talks#upcoming_talks', as: 'upcoming_talks'
  get 'recent_talks' => 'talks#recent_talks', as: 'recent_talks'
  get 'about_me' => 'talks#about_me', as: 'about_me'
  get 'search' => 'talks#search'
  get 'talks/:id/addslide' => 'talks#add_slide', as: 'add_slide'
  match 'download_slide/:id', to: 'talks#download_slide', as: 'download_slide', via: :get

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
