# frozen_string_literal: true

class ReferencesController < ApplicationController
  def create
    @talk = Talk.find(params[:talk_id])
    @reference = @talk.references.create(reference_params)
    redirect_to talk_path(@talk)
  end

  def destroy
    @talk = Talk.find(params[:talk_id])
    @reference = @talk.references.find(params[:id])
    @reference.destroy
    flash[:danger] = 'Reference deleted!'
    redirect_to talk_path(@talk)
  end

  private

  def reference_params
    params.require(:reference).permit(:body)
  end
end
