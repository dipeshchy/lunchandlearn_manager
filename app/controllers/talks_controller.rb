# frozen_string_literal: true

class TalksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_talk, only: %i[show edit update destroy add_slide]

  # method to get list of all talks from past and future
  def index
    @talks = Talk.all.order(date: :desc)
  end

  # method to get list of all talks upcoming talks (future)
  def upcoming_talks
    @talks = Talk.where('date > ?', DateTime.now).order(date: :asc)
  end

  # method to get list of all talks recent talks (past)
  def recent_talks
    @talks = Talk.where('date < ?', DateTime.now).order(date: :asc)
  end

  # method to get search results
  def search
    @search_term = params[:search]
    @talks = Talk.where('title like ?', "%#{@search_term}%")
  end

  # This method will redirect to form to create a new talk event
  def new
    @talk = Talk.new
  end

  # method to insert data to database(create)
  def create
    @user = current_user
    @talk = @user.talks.new(talk_params)
    if @talk.save
      flash[:success] = 'Talk Added Successfully!'
      redirect_to @talk
    else
      render 'new'
    end
  end

  # method to show individual talk. It receives @talk variable from set_talk method called before action
  def show; end

  # method to redirect to edit page. It receives @talk variable from set_talk method called before action
  def edit; end

  # method to update talk details. It receives @talk variable from set_talk method called before action
  def update
    if @talk.update(talk_params)
      flash[:success] = 'Talk Updated!'
      redirect_to @talk
    else
      render 'edit'
    end
  end

  # method to delete the talk. It receives @talk variable from set_talk method called before action
  def destroy
    @talk.destroy
    flash[:danger] = 'Talk Deleted!'
    redirect_to root_path
  end

  # method to redirect user to his profile
  def about_me; end

  # method to redirect to add slide page. It receives @talk variable from set_talk method called before action
  def add_slide; end

  # method to download the slide
  def download_slide
    download_file = Talk.find(params[:id]).slide
    send_file download_file.path, status: 202
  end

  # method for security of input params
  private

  def talk_params
    params.require(:talk).permit(:title, :location, :date, :description, :slide)
  end

  # method which is common for all to set talk by finding id
  def set_talk
    @talk = Talk.find(params[:id])
  end
end
