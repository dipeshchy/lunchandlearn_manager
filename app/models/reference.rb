# frozen_string_literal: true

class Reference < ApplicationRecord
  belongs_to :talk
  validates :body, presence: true
end
