# frozen_string_literal: true

class Talk < ApplicationRecord
  belongs_to :user
  has_many :references, dependent: :destroy
  mount_uploader :slide, SlideUploader
  validates :title, presence: true
  validates :date, presence: true
  validates :location, presence: true
  validates :description, presence: true
end
