# Lunch And Learn Manager
This project is for the management of lunch and learn event. An organiser can schedule an event or meeting where people can come,eat,meet and learn. This application meant to manage the scheduling,and provide informations to the
peoples.
This project have the following functionality :-
   * Login system
   * Home Page for Admin and as well as User.
   * Admin can invite users.
   * Admin/User can schedule an event,search event.
   * User can upload slide for the the meeting.
As from the user story , I have prioritized users have to add and search the meetinh and upload slide.
## Technologies used
Different technologies were used in order to complete the project. They are all listed below :-
### 1. Visual Studio Code(VS code)
Visual Studio code was used as a text editor in the project. It is a source-code editor developed by Microsoft for Windows, Linux and macOS.
### 2. Wireframe
A website wireframe,also known as a page schematic or screen blueprint,is a visual guide that represents the skeletal framework of a website.
### 3. Rails
The application used Rails 6.0.0 as its framework.Rails is a model–view–controller (MVC) framework, providing default structures for a database, a web service, and web pages. It is based on Ruby programming language.
### 4. Bootstrap
This is framework od CSS used for styling.
### 5. Mysql
Mysql was the database used for the application. 

The tables used in projects are:-
#### 1. devise
I have used a gem namley devise which is for the purpose providing authentications.
#### 2. devise_invitable
I have used a gem namley devise_invitable which is for the purpose of inviting other users.
#### 3. users
It consist of email and username as its field.
#### 4. talk
It consists of title,location,date,description,slide as its field. It uses user_id as references.
## Gems used
RubyGems is a package manager for the Ruby programming language that provides a standard format for distributing Ruby programs and libraries, a tool designed to easily manage the installation of gems, and a server for distributing them. The gems used in the application are:-
### 2. carrierwave
It is used for file uploading.
### 2. devise
Devise is a flexible authentication solution for Rails based on Warden. It was used for user authentication.
### 3. devise_invitable
The gem was used to send invitation to the users.
### 4. pry-rails
Pry is a runtime developer console and IRB alternative with powerful introspection capabilities.
I have used a gem namley devise_invitable which is for the purpose of inviting other users.