# frozen_string_literal: true

class CreateReferences < ActiveRecord::Migration[6.0]
  def change
    create_table :references do |t|
      t.text :body
      t.references :talk, null: false, foreign_key: true

      t.timestamps
    end
  end
end
