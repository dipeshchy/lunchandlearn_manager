# frozen_string_literal: true

class AddSlideToTalks < ActiveRecord::Migration[6.0]
  def change
    add_column :talks, :slide, :string
  end
end
